Geoptik_Nadirus_Cell
====================

3D printable part for modify the mirror support of a Geoptik Nadirus 12" telescope.

# The issue

The Geoptik Nadirus 12" telescope has decent mirror cell, but the
same can't be said about the edge mirror support.

For this telescopes, the edge support are simple plastic cylinders
forming an equilateral triangle.
![original edge support](media/original_support.jpg).

This simple approach for the mirror edge support is quite
insufficient. In particular, it results in a collimation shift when
the altitude is changed: You collimate the telescope, then you move its
altitude axis and the collimation is broken...

# The solution

It is necessary to change the mirror edge support in order to permit
to the mirror move and permits to the cell to do its work.

Usually, the mirror edge support of a mirror in a Dobson telescope
is made with a wire or ribbon that supports the mirror "hanging", so
the mirror can move and the cell can do its work.

A more modern approach use plastic bearings for rest the mirror and
allow it move with the freedom that the cell requires for working
properly.

Two bearing are required for support the mirror edge. Experts found
that a separation forming and angle of 90 degree between them are the
better design. But others layouts, as a separation of 120 degrees,
give acceptable results, specially for mirrors not too thin.

Pondering different solutions and due I want not make
"no-reversible" changes in the telescope itself, I finally decide
replace the 3 original plastic cylinders of the mirror edge support
by a 3D printed parts. Two of these part shall hold bearings 120 degrees
apart in which the weight of the mirror should rest, and a third part simply
should secure the mirror when transporting.

# Hands on

## Bearings

Plastic (POM) bearings must be used.

There are two common sizes used in the 3D printer world. The small ones fits
the requirements. The size is 14mm in diameter, 9.5mm thickness and hole of
5mm.

[I bought mine here](https://www.amazon.es/gp/product/B083ZTPJB4) (the small
ones), but seems to be discontinued. I think that the model is MR105 (with POM
covert)

I THINK that [these are as mine](https://es.aliexpress.com/item/32998962693.html) 

## 3D printed parts

I designed two flavours for the printed parts: one have thread and other use
metallic inserts with threads.

Since the 3D printers usually are not able to make functional M8 and M6 threads
I strongly recommend use the not thread version (this is, whit metallic inserts)
unless you know what you're doing.

You must print two pieces with bearing hole (for the bottom of the mirror)
and one without bearing hole for the upper of the mirror.

I printed mines in solid (100% infill) black ABS.

## Other parts

### Metallic inserts

For the not thread versioned you will need M6 and M8 steel inserts
[as these](https://www.amazon.es/gp/product/B07B8GMZ3M), in total 3 of each.

### List of material

You will need:
 - 3x M8 threaded inserts (no threaded 3D part only)

 - 3x M6 threaded inserts (no threaded 3D part only)

 - 3x countersunk bolts allen type size M8x35mm
   (A2 steel, preferably).

 - 3x countersunk bolts allen type size M6x20mm
   (A2 steel, preferably).

 - 2x hexagonal head bolts M5x35mm (A2 steel, preferably).

 - 2x normal nuts M5 (A2 steel, preferably).

 - 4x narrow washers M5 (A2 steel, preferably).

 - 4x self-locking nuts M5 (A2 steel, preferably). You must remove the nylon
   ring.

 - 4x rubber o-ring 12.1x17.5x2.7 (Nº10)

 - 4x threaded rod M3x90mm

 - 8x self-locking nuts M3

 - 8x washers M3

 - Heat shrink tubing 4mm aprox. in diameter.

 - Adhesive thin felt.

## Making the supports

I will assume that the non threaded version of the 3D parts are the used ones.

My parts are printed in ABS with 90~100% infill.

For both the bottom (2) and upper (1) supports you must fix the metallic threaded
inserts with epoxy glue.

This picture show how to mount the M5x35 bolt, nuts, washer and bearing. Please
note the orientation of the M5 self-locking nuts on either side of bearing.
Remember that the nylon ring of the M5 self-locking nut must be removed. What
we want is that that both sides of the bearing lies over the curved part of the
self-locking M5 nuts.
![bearing_mount](media/bearing_mount.jpg)

For the bottom supports, placement of the bearing have a trick: you must put
the internal lock nuts (without the nylon rings) in such a way that the pin
(M5x35) bolt throw gently AND the nuts rest without protruding (except the
rounded end). You must use "try and failure" method turning the position of
the nuts in its holes until you get it. May be you need to file a few the flat
part of the M5 self-locking nuts for a perfect adjustment: 3D printers
usually have not to much dimensional accuracy.

Another trick is how to glue (epoxy based glue recommended) the M8 inserts
into the bottom parts. Before glue them, you should mark the turning position
when they rest screwed in but not tight, so you can turn the inserts for the
M8 bolts before glue them for procure a correct position of the support
(bearings in  in front of the mirror) when screwed. You can tuning the final
position with the M8 bolt, but the described procedure improves the results.

![marking support](media/marking_support.jpg)

![marking insert](media/marking_insert.jpg)

![glue insert](media/glue_insert.jpg)

## Prepare the anti-roll heads

Put adhesive thin felt in the bottom of the original plastic anti-roll pieces
(the side that looks to the mirror surface).

![anti-roll heads](media/antiroll_heads.jpg)

## Placement of the supports

Screw the supports in its position using the M8x35 bolts. Use the "counter
thread" method for fix the supports in their positions.

![put support](media/put_support.jpg).

For the bottom supports only, use the M3x90 threaded rod for reinforce its
position and avoid the support to turn, as in the image:

![bottom support reinforce](media/support_reinfor.jpg)

## Final mount

Filially, put the mirror in site and screw the antiroll pieces with the M6x20
bolts.

![final mount 1](media/final_mount-1.jpg)

![final mount 2](media/final_mount-2.jpg)

You can take a look to the short video "mounted_supports.mp4" in the "media"
folder.

As a last note, I want to clarify that only the two bottom supports have
bearing mounted in them, for rest the mirror when the telescope are not
pointing to the zenith. The (alone) upper support has no bearing and only serves
for maintain the mirror in site during the transport. When you are using the
telescope, this upper support must NOT touch the mirror. In this way, the mirror
will have freedom for slide over the bottom bearings and the cell can work
properly. Sorry: I have no pictures of the upper support after mounted.

## Licence

This work is released under the terms of creative commons
"Attribution 4.0 International".

You are free to:

Share — copy and redistribute the material in any medium or format.

Adapt — remix, transform, and build upon the material for any purpose, even
commercially.

The licensor cannot revoke these freedoms as long as you follow the license
terms.

More details in the "licence.txt" file.

