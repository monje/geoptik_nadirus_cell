Mirror height rule
==================

This auxiliar 3D piece servers as a template for mark a line along the mirror
edge to point where the lateral COG (center of gravity) is.

**This rule is not necessary unless you want to test that the supports fits
your mirror tickness**

The calculation of the lateral COG can be calculed using the
[Mirror Edge Support Calculator](http://www.cruxis.com/scope/mirroredgecalculator.htm)

For example, for a mirror of 300 mm diameter, f/5 and 37 mm thickness the
lateral COG is at 17.58 mm from the bottom of the mirror.

We must also take into account the thickness of the pen we will use for mark
the line. It is also recommended to adjust this alture to an integer number of
times the layer height of the 3D printer.

So for a 17.58 mm height and 0.5 mm pen thickness the value to use will be:

17.58-(0.5/2)=17.33

And usind a layer height of 0.2 mm we can round this value to 17.20 (=86*0.2)


Lastly, since the full template would not fit on the printer bed, only a
section of 90 degrees is printed.

![Mirror height rule](media/mirror_height_rule.jpg)

